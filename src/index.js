import React from "react";
import ReactDOM from "react-dom/client";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import { Provider } from "react-redux";
import store from "./redux/store";
import App from "./App";
import CartPage from "./pages/CartPage";
import FavouritesPage from "./pages/FavouritesPage";
import Errorpage from "./pages/Errorpage";

const root = ReactDOM.createRoot(document.getElementById("root"));

export default function Index() {
  return (
    <Provider store={store}>
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<App />} />
          <Route path="/CartPage" element={<CartPage />} />
          <Route path="/FavouritesPage" element={<FavouritesPage />} />
          <Route path="*" element={<Errorpage />} />
        </Routes>
      </BrowserRouter>
    </Provider>
  );
}

root.render(<Index />);